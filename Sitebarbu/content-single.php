<article class="post-content section-box">
    <div class="post-inner">
        <header class="post-header">
            <div class="post-data">
                <div class="post-tag">
                    <a href="single.html">#Photo</a>
                    <a href="single.html">#Architect</a>
                </div>

                <div class="post-title-wrap">
                    <h1 class="post-title"><?php the_title() ?></h1>
                    <time class="post-datetime" datetime="2015-03-13T07:44:01+00:00">
                        <span class="day"><?php echo get_the_date('d') ?></span>
                        <span class="month"><?php echo get_the_date('M') ?></span>
                    </time>
                </div>

                <div class="post-info">
                    <a href="single.html"><i class="rsicon rsicon-user"></i>by <?php the_author() ?></a>
                    <a href="single.html"><i class="rsicon rsicon-comments"></i>56</a>
                </div>
            </div>
        </header>

        <div class="post-editor clearfix">
            <?php the_content()?>
        </div>

        <?php the_field('image-background','option'); ?>

        <footer class="post-footer">
            <div class="post-share">
                <script type="text/javascript" src="https://s7.addthis.com/js/300/addthis_widget.js#pubid=ra-503b5cbf65c3f4d8" async="async"></script>
                <div class="addthis_sharing_toolbox"></div>
            </div>
        </footer>
    </div><!-- .post-inner -->
</article><!-- .post-content -->
<?php
add_action('wp_enqueue_scripts', 'custom_scripts');
function custom_scripts(){
    wp_enqueue_style('bxslider', get_template_directory_uri() . '/js/plugins/jquery.bxslider/jquery.bxslider.css');
    wp_enqueue_style('mCustomScrollbar', get_template_directory_uri() . '/js/plugins/jquery.customscroll/jquery.mCustomScrollbar.min.css');
    wp_enqueue_style('mediaelementplayer', get_template_directory_uri() . '/js/plugins/jquery.mediaelement/mediaelementplayer.min.css');
    wp_enqueue_style('fancybox', get_template_directory_uri() . '/js/plugins/jquery.fancybox/jquery.fancybox.css');
    wp_enqueue_style('carousel', get_template_directory_uri() . '/js/plugins/jquery.owlcarousel/owl.carousel.css');
    wp_enqueue_style('theme', get_template_directory_uri() . '/js/plugins/jquery.owlcarousel/owl.theme.css');
    wp_enqueue_style('option-panel', get_template_directory_uri() . '/js/plugins/jquery.optionpanel/option-panel.css');
    wp_enqueue_style('theme-color', get_template_directory_uri() . '/colors/theme-color.css');
    wp_enqueue_style('style', get_template_directory_uri() . '/style.css');
}

add_action('after_setup_theme', 'custom_theme_setup');
function custom_theme_setup()
{
    //WordPress Titles
    add_theme_support('title-tag');

    add_theme_support('post-thumbnails');


    register_nav_menus(array(
        'primary' => 'Menu principal',
    ));
}
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}
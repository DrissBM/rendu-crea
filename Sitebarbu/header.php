<!DOCTYPE html>
<html lang="en" class=" theme-color-07cb79 theme-skin-light">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- Favicon -->
    <link rel="shortcut icon" type="image/ico" href="<?php echo get_bloginfo('template_directory'); ?>/img/favicon.png" />

    <!-- Google Fonts -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Fredoka+One">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic">

    <!-- Icon Fonts -->
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/fonts/map-icons/css/map-icons.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_directory'); ?>/fonts/icomoon/style.css">

    <!-- Styles -->



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

    <!-- Modernizer for detect what features the user’s browser has to offer -->
    <script type="text/javascript" src="<?php echo get_bloginfo('template_directory'); ?>/js/libs/modernizr.js"></script>
    <?php wp_head() ?>
</head>

<body class="loading header-has-img">

    <?php get_template_part('mobile_nav'); ?>

    <?php get_sidebar(); ?> 

    <div class="wrapper">
        <header class="header">
            <div class="head-bg" style="background-image: url('<?php the_field('image-background','option'); ?>')"></div>

            <div class="head-bar">
                <div class="head-bar-inner">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <a class="logo" href="<?php echo home_url() ?>"><span>RS</span>card</a>
                            <!-- <a class="head-logo" href=""><img src="<?php echo get_bloginfo('template_directory'); ?>/img/rs-logo.png" alt="RScard"/></a> -->
                        </div>

                        <div class="col-sm-9 col-xs-6">
                            <div class="nav-wrap">
                                <nav id="nav" class="nav">
                                    <?php
                                    wp_nav_menu(array(
                                        'theme_location' => 'primary',
                                        'container' => false,
                                        'menu_class' => 'clearfix',
                                    ));
                                    ?>
                                </nav>

                                <button class="btn-mobile btn-mobile-nav">Menu</button>
                                <button class="btn-sidebar btn-sidebar-open"><i class="rsicon rsicon-menu"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header><!-- .header -->
<?php get_header(); ?>

<div class="content">
    <div class="container">

        <!-- START: PAGE CONTENT -->
        <div class="blog">
            <div class="blog-grid">
                <div class="grid-sizer"></div>

                <?php
                if (have_posts()) : while (have_posts()) : the_post();
                        get_template_part('content');
                    endwhile;
                endif;
                ?>
            </div><!-- .blog-grid -->

            <div class="pagination">
                <a class="next page-numbers" href=""><i class="rsicon rsicon-chevron_left"></i></a>
                <?php next_post_link('<') ?>
                <span class="page-numbers current">1</span>
                <a class="page-numbers" href="category.html">2</a>
                <a class="page-numbers" href="category.html">3</a>
                <span>...</span>
                <a class="page-numbers" href="category.html">22</a>
                <?php previous_post_link('>') ?>
                <a class="next page-numbers" href="category.html"><i class="rsicon rsicon-chevron_right"></i></a>
            </div><!-- .pagination -->
        </div><!-- .blog -->
        <!-- END: PAGE CONTENT -->

    </div><!-- .container -->
</div><!-- .content -->

<?php get_footer(); ?>
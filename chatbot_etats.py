#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Simple Bot to reply to Telegram messages
# This program is dedicated to the public domain under the CC0 license.
"""
This Bot uses the Updater class to handle the bot.
First, a few callback functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.
Usage:
Example of a bot-user conversation using ConversationHandler.
Send /start to initiate the conversation.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove, ParseMode)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

import sys, logging, requests, time, math

bot_token = sys.argv[1] #Le premier paramètre (0) étant le nom du script (fichier)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

START, SORTIR_CHOIX, RESTAURANTS_CHOIX, RESTAU_RESULTATS, CARTES, RETOUR, TRANSPORT= range(7)


def start(bot, update):
    reply_keyboard = [['Restaurants', 'Sorties']]

    update.message.reply_text(
        'Hi! My name is Professor Bot. Je suis la pour vous aiguiller sur les choix que vous pourriez avoir pour ce soir.\n\n'
        'Que voulez vous faire ce soir ? Un restaurant ou une petite sortie ?''\nPour stopper la discussion tapez /cancel',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return START

def sortir_choix(bot, update):
    reply_keyboard = [['Musées', 'Bars', 'Clubs', 'Restaurants', 'Retour']]

    user = update.message.from_user
    logger.info("Volontée de %s: %s", user.first_name, update.message.text)
    update.message.reply_text(
        'Je vois. ce soir vous voulez vous faire une petite sortie, '
        'Voici donc les choix que nous vous proposons pour ce soir. ''\nPour stopper la discussion tapez /cancel',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return SORTIR_CHOIX

def musees(bot, update):
    reply_keyboard = [['Retour']]

    update.message.reply_text(
        'Oh vous voulez aller au musée ?.\n'
        'Voici le top 3 des musées :\n\nMuseum d\'ethnographie de Genève\nFondation Baur Musée des arts d\'Extrême-Orient\nMuséum d\'histoire naturelle de Genève'
        '\nPour stopper la discussion tapez /cancel',
        reply_markup = ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RETOUR

def bars(bot, update):
    reply_keyboard = [['Retour']]

    update.message.reply_text(
        'Alors comme ça on veut sortir boir un verre ?.\n\n'
        'Je te conseil ces 3 bars :\n\nBoulevard du vin\n\t\tBoulevard Georges-Favon 3\n\nPAV Bar\n\t\t1227 Allée H, Route des Jeunes 27\n\nMeltdown Genève\n\t\tRoute des Acacias'
        '\nPour stopper la discussion tapez /cancel',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RETOUR

def clubs(bot, update):
    reply_keyboard = [['Retour']]

    update.message.reply_text(
        'Je connais les meilleurs club du paff.\n'
        'Voici ceux que je te conseil :\n\nL\'usine Opéra\nPoint Bar Club\nSilencio Club''\nPour stopper la discussion tapez /cancel',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RETOUR

def restaurant_choix(bot, update):
    reply_keyboard = [['Asiate', 'Kebab', 'Pizza', 'Burger', 'Pates', 'Retour']]

    user = update.message.from_user
    logger.info("%s a choisis %s", user.first_name, update.message.text)
    update.message.reply_text(
        'Vous voulez vous faire une bonne bouffe, '
        'Voilà donc les choix que nous vous proposons pour ce soir.''\nPour stopper la discussion tapez /cancel',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RESTAURANTS_CHOIX

def asiate(bot, update):
    reply_keyboard = [['Restaurant1','Restaurant2','Restaurant3','Restaurant4','Restaurant5', 'Retour']]

    update.message.reply_text(
        'Voici les restau dispo.\n\n'
        'Quelle carte voulez vous lire ?''\nPour stopper la discussion tapez /cancel',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return RESTAU_RESULTATS

def restau1(bot, update):
    reply_keyboard = [['Autres restaurants']]
    update.message.reply_location(48.862725,2.287592)
    update.message.reply_text(
        'Voici l\'adresse du restaurant rechercher.\n\n'
        'Voulez vous en voir un autre ?''\nPour stopper la discussion tapez /cancel',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))
    return CARTES

def appeler_opendata(path):
    url = "http://transport.opendata.ch/v1/"+path
    print(url)
    reponse = requests.get(url)
    return reponse.json()

def calcul_temps_depart(timestamp):
    seconds = timestamp-time.time()
    minutes = math.floor(seconds/60)
    if minutes < 1:
        return "FAUT COURIR!"
    if minutes > 60:
        return "> {} h.".format(math.floor(minutes/60))
    return "dans {} min.".format(minutes)

def afficher_arrets(update,arrets):
    texte_de_reponse= "Voici les arrets:\n"

    for station in arrets["stations"]:
        if station["id"]is not None:
            texte_de_reponse += "\n/a" + station['id'] + " " + station['name']

    update.message.reply_text(texte_de_reponse)

def afficher_departs(update, departs):
    texte_de_reponse = "Voici les prochains departs:\n\n"
    for depart in departs['stationboard']:
        texte_de_reponse += "{} {} dest. {} - {}\n".format(
            depart['category'],
            depart['number'],
            depart['to'],
            calcul_temps_depart(depart['stop']['departureTimestamp'])
        )
    texte_de_reponse += "\nAfficher a nouveau: /a" + departs['station']['id']

    coordinate = departs['station']['coordinate']
    update.message.reply_location(coordinate['x'], coordinate['y'])
    update.message.reply_text(texte_de_reponse)


def bienvenue(bot, update):
    update.message.reply_text("Merci d'envoyer votre localisation (via piece jointe ou simplement en texte)",
    reply_markup = ReplyKeyboardRemove())
    return TRANSPORT

def lieu_a_chercher(bot, update):
    resultats_opendata = appeler_opendata("locations?query="+update.message.text)
    afficher_arrets(update, resultats_opendata)
    return TRANSPORT

def coordonnees_a_traiter(bot, update):
    location=update.message.location
    resultats_opendata = appeler_opendata("locations?x={}&y={}".format(location.latitude, location.longitude))
    afficher_arrets(update, resultats_opendata)
    return TRANSPORT

def details_arret(bot, update):
    arret_id = update.message.text[2:]
    afficher_departs(update, appeler_opendata("stationboard?id=" + arret_id))
    return TRANSPORT

def cancel(bot, update):
    user = update.message.from_user
    logger.info("User %s canceled the conversation.", user.first_name)
    update.message.reply_text('Bye! I hope we can talk again some day.',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END

def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)

def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(bot_token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Add conversation handler with the states GENDER, PHOTO, LOCATION and BIO

    conv_handler_discussion = ConversationHandler(
        entry_points=[CommandHandler('start', start)],

        states={
            START:
                [
                    RegexHandler('^(Sorties)$', sortir_choix),
                    RegexHandler('^(Restaurants)$', restaurant_choix)
                ],

            SORTIR_CHOIX:
                [
                    RegexHandler('^(Musées)$', musees),
                    RegexHandler('^(Bars)$', bars),
                    RegexHandler('^(Clubs)$', clubs),
                    RegexHandler('^(Restaurants)$', restaurant_choix),
                    RegexHandler('^(Retour)$', start)
                ],
            RETOUR:
                [
                    RegexHandler('^(Retour)$', sortir_choix)
                ],

            RESTAURANTS_CHOIX:
                [
                    RegexHandler('^(Asiate)$', asiate),
                    RegexHandler('^(Kebab)$', asiate),
                    RegexHandler('^(Pizza)$', asiate),
                    RegexHandler('^(Burger)$', asiate),
                    RegexHandler('^(Pates)$', asiate),
                    RegexHandler('^(Retour)$', start)
                ],

            RESTAU_RESULTATS:
                [
                    RegexHandler('^(Restaurant1)$', restau1),
                    RegexHandler('^(Restaurant2)$', restau1),
                    RegexHandler('^(Restaurant3)$', restau1),
                    RegexHandler('^(Restaurant4)$', restau1),
                    RegexHandler('^(Restaurant5)$', restau1),
                    RegexHandler('^(Retour)$', restaurant_choix)
                ],

            CARTES:
                [
                    RegexHandler('^(Autres restaurants)$', asiate),
                ],




        },

        fallbacks=[CommandHandler('cancel', cancel)]

    )

    conv_handler_transport = ConversationHandler(
        entry_points=[CommandHandler('transport', bienvenue)],

        states={
            TRANSPORT:
                [
                    (MessageHandler(Filters.text, lieu_a_chercher)),
                    (MessageHandler(Filters.location, coordonnees_a_traiter)),
                    (MessageHandler(Filters.command, details_arret))
                ],

        },



        fallbacks=[CommandHandler('cancel', cancel)]

    )
    dp.add_handler(conv_handler_discussion)
    dp.add_handler(conv_handler_transport)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()